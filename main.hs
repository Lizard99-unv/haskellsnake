import Data.Map as Map
import System.Random
import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game

data Direction = UP | DOWN | LEFT | RIGHT deriving (Eq, Ord)
type Food = (Int, Int)
type Snake = [Food]

cols = 32
rows = 24

directionVectorMap = Map.fromList $ zip [UP, DOWN, LEFT, RIGHT] 
                                        [(0, (-1)), (0, 1), ((-1), 0), (1, 0)]

move :: Food -> Direction -> Snake -> (Bool, Snake)
move food direction snake = if wasFoodEaten 
                            then (True, newHead : snake)
                            else (False, newHead : init snake)
    where   wasFoodEaten = newHead == food
            newHead = directionVectorMap ! direction +: head snake
            (a, b) +: (c, d) = ((mod (a + c) 32), (mod (b + d) 24))

checkGameOver :: Snake -> Bool
checkGameOver snake = elem (head snake) (tail snake)

generateNewFood :: Snake -> StdGen -> (Food, StdGen)
generateNewFood snake stdGen =  if elem newFood snake
                                then generateNewFood snake stdGen3
                                else ((foodX, foodY), stdGen3)
        where   (foodX, stdGen2) = randomR (1, 31) stdGen
                (foodY, stdGen3) = randomR (1, 23) stdGen2
                newFood = (foodX, foodY)

data GameState = GameState      { getSnake :: Snake
                                , getFood :: Food
                                , getDirection :: Direction
                                , isGameOver :: Bool
                                , getRandomStdGen :: StdGen }

changeDirection :: GameState -> Direction -> GameState
changeDirection (GameState s f d g r) newDir = GameState s f newDir g r 

initialGameState gameOver = GameState   { getSnake = [  (snakeX, snakeY), 
                                                        (snakeX, snakeY - 1)]
                                        , getFood = (3, 3)
                                        , getDirection = DOWN
                                        , isGameOver = gameOver
                                        , getRandomStdGen = mkStdGen 100 }
        where   snakeX = div cols 2
                snakeY = div rows 2

window :: Display
window = InWindow "Haskell Snake Game" (640, 480) (100, 100)

background :: Color
background = black

render :: GameState -> Picture
render gameState = pictures $     fmap (convertToPicture green) snake ++ 
                                  fmap (convertToPicture red) [food] ++
                                  score ++
                                  gameOverPicture
    where   snake = getSnake gameState 
            food = getFood gameState
            convertToPicture :: Color -> (Int, Int) -> Picture
            convertToPicture color' (x, y) = fillRectangle color' (toFloat (x, y)) (20, 20)
            fillRectangle color' (tx, ty) (w, h) =  color color' $ 
                                                    scale 1 (-1) $ 
                                                    translate (tx * 20 - 320) (ty * 20 - 240) $ 
                                                    rectangleSolid w h
            toFloat (x, y) = (fromIntegral x, fromIntegral y)
            score = [color white $ 
                    translate (-280) (200) $ 
                    scale 0.1 0.1 $ 
                    text ("SCORE: "++ (show (((length snake) - 2) * 10)))]
            gameOverPicture =   if (isGameOver gameState) 
                                then [  color white $ 
                                        translate (-200) (0) $ 
                                        scale 0.5 0.5 $ 
                                        text "GAME OVER"
                                     ,  color white $ 
                                        translate (-175) (-50) $ 
                                        scale 0.2 0.2 $ 
                                        text "Press SPACE to try again." 
                                     ,  color white $ 
                                        translate (-180) (-100) $ 
                                        scale 0.2 0.2 $ 
                                        text "Use WASD to control snake."    ] 
                                else []
                                                        
update :: Float -> GameState -> GameState
update seconds gameState =  if (gameOver) 
                            then gameState
                            else GameState newSnake newFood' direction newGameOver newStdGen
    where   snake = getSnake gameState 
            food = getFood gameState
            direction = getDirection gameState
            gameOver = isGameOver gameState
            stdGen = getRandomStdGen gameState
            (wasFoodEaten, newSnake) = move food direction snake
            (newFood, newStdGen) = generateNewFood newSnake stdGen
            newFood' =  if wasFoodEaten 
                        then newFood
                        else food
            newGameOver = checkGameOver newSnake

handleKeys :: Event -> GameState -> GameState
handleKeys (EventKey (Char 'a') Down _ _) (GameState a b RIGHT c d) = (GameState a b RIGHT c d)
handleKeys (EventKey (Char 'a') Down _ _) gameState = changeDirection gameState LEFT
handleKeys (EventKey (Char 'd') Down _ _) (GameState a b LEFT c d) = (GameState a b LEFT c d)
handleKeys (EventKey (Char 'd') Down _ _) gameState = changeDirection gameState RIGHT
handleKeys (EventKey (Char 'w') Down _ _) (GameState a b DOWN c d) = (GameState a b DOWN c d) 
handleKeys (EventKey (Char 'w') Down _ _) gameState = changeDirection gameState UP
handleKeys (EventKey (Char 's') Down _ _) (GameState a b UP c d) = (GameState a b UP c d) 
handleKeys (EventKey (Char 's') Down _ _) gameState = changeDirection gameState DOWN 
handleKeys (EventKey (SpecialKey KeySpace) Down _ _) gameState =    if (isGameOver gameState)
                                                                    then initialGameState False
                                                                    else gameState
handleKeys _ gameState = gameState

main :: IO ()
main = play window background 10 (initialGameState True) render handleKeys Main.update

